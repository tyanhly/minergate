<?php


$ips = [
  'toanproject' => '104.198.208.155',
  'tungtest-1131' => '35.190.157.99',
  'consultantkiss' => '35.189.67.88',
  'kiss-map' => '35.197.31.32',

//   'project11' => '104.154.108.127',
//   'project12' => '104.197.162.2',
//   'project13' => '104.198.50.198',
//   'project14' => '146.148.50.144',
//   'project15' => '35.184.225.250',
  


//   'project1' => '35.184.109.200',
//   'project2' => '35.188.75.33',
//   'project3' => '35.188.60.126',
//   'project4' => '35.184.205.125',
// #2
//   'project34' => '104.198.252.200',
//   'project32' => '35.188.173.205',
//   'project33' => '35.188.81.46',
//   'project31' => '104.197.0.10',
];


$ip1s = [

  'kiss' => '35.189.82.15',
  'project31' => '104.197.0.10',
];


$sshRsaId = '/.ssh/id_rsa';
// sudo cp .ssh/ / -r
// sudo chmod 400 .ssh/id_rsa
// sudo chown vagrant:vagrant -R .ssh


function setup($ip, $project){
	global $sshRsaId;
	$setupCmd = 'sudo apt-get install htop -y && wget https://minergate.com/download/deb-cli -O minergate-cli.deb && sudo dpkg -i minergate-cli.deb';
	echo "\nSetup project: $project \n";
	$cmd = "ssh -i $sshRsaId MyPC@$ip '$setupCmd'";
	echo $cmd . "\n";
	echo shell_exec($cmd );
}

function start($ip, $project){
	global $sshRsaId;
	echo "\nDatetime:" . date('Ymd h:i:s') . "\n";
	echo "\nStart mining project: $project\n";
	// $cmd = "ssh $ip 'minergate-cli -user tyanhly@gmail.com -bcn 1 -fcn+xmr 2  -qcn 1 -xdn 1 -dsh 1 -mcn 1 > /dev/null 2>&1 &'";
	$cmd = "ssh -i $sshRsaId MyPC@$ip 'minergate-cli -user tyanhly@gmail.com -fcn+xmr 7 > /dev/null 2>&1 &'";
	echo $cmd . "\n";
	echo shell_exec($cmd . " > /dev/null 2>&1 &");
}

function stop($ip, $project){
	global $sshRsaId;
	echo "\nDatetime:" . date('Ymd h:i:s') . "\n";
	echo "\nStop mining project: $project\n";
	$cmd = "ssh -i $sshRsaId MyPC@$ip 'kill -TERM `pidof minergate-cli`'";

	echo $cmd . "\n";
	echo shell_exec($cmd . " > /dev/null 2>&1 &");
}

function restart($ip, $project){
	global $sshRsaId;
	echo "\nDatetime:" . date('Ymd h:i:s') . "\n";	
	stop($ip,$project);
	sleep(2);
	start($ip,$project);
}


function setup1($ip, $project){
	global $sshRsaId;
	$setupCmd = "sudo DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:ethereum/ethereum-qt -y && sudo DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:ethereum/ethereum -y && sudo apt-get update && sudo apt-get install ethminer htop git python-twisted -y && git clone https://github.com/tyanhly/eth-proxy && cd eth-proxy";
	echo "\nSetup project: $project \n";
	$cmd = "ssh -i $sshRsaId MyPC@$ip '$setupCmd'";
	echo $cmd . "\n";
	echo shell_exec($cmd );
}

function start1($ip, $project){
	global $sshRsaId;
	// echo "ssh -i $sshRsaId MyPC@$ip";return;
	// echo "\nDatetime:" . date('Ymd h:i:s') . "\n";
	// echo "\nStart mining project: $project\n";
	
	// $cmd = "ssh -i $sshRsaId MyPC@$ip 'cd ~/eth-proxy && git reset --h'";
	// echo $cmd . "\n";
	// echo shell_exec($cmd);
	
	// $cmd1 = "ssh -i $sshRsaId MyPC@$ip 'cd ~/eth-proxy && python ./eth-proxy.py  > /dev/null 2>&1 &' > /dev/null 2>&1 &";
	// echo $cmd1 . "\n";
	// echo shell_exec($cmd1);
	
	// $cmd2 = "ssh -i $sshRsaId MyPC@$ip 'ethminer -t 6 --farm-recheck 200 -C -F http://127.0.0.1:8080/$project  > /dev/null 2>&1 &' > /dev/null 2>&1 &";
	$cmd2 = "ssh  -i $sshRsaId MyPC@$ip 'ethminer -t 7 -C -F http://eth.pool.minergate.com:55751/tyanhly@gmail.com --disable-submit-hashrate > /dev/null 2>&1 &'";
	
	echo $cmd2 . "\n";
	echo shell_exec($cmd2 . " > /dev/null 2>&1 &");
}

function stop1($ip, $project){
	global $sshRsaId;
	echo "\nDatetime:" . date('Ymd h:i:s') . "\n";
	echo "\nStop mining project: $project\n";
	$cmd1 = "ssh -i $sshRsaId MyPC@$ip 'kill -TERM `pidof ethminer`'";
	$cmd2 = "ssh -i $sshRsaId MyPC@$ip 'kill -TERM `pidof python`'";

	echo $cmd1 . "\n";
	echo shell_exec($cmd1 . " > /dev/null 2>&1 &");
	echo $cmd2 . "\n";
	echo shell_exec($cmd2 . " > /dev/null 2>&1 &");
}

function restart1($ip, $project){
	global $sshRsaId;
	echo "\nDatetime:" . date('Ymd h:i:s') . "\n";	
	stop1($ip,$project);
	sleep(2);
	start1($ip,$project);
}