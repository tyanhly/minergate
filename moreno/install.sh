# get git to install it
sudo apt-get install git -y

# dependencies
sudo apt-get install build-essential autotools-dev autoconf libcurl3 libcurl4-gnutls-dev -y
cd ~
# download latest version
git clone https://github.com/wolf9466/cpuminer-multi

cd cpuminer-multi/

# compile
./autogen.sh
CFLAGS="-march=native" ./configure
make

#install
sudo make install

echo 'cd ~/cpuminer-multi' >> ~/.bashrc
